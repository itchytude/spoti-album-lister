# README #

A simple album/playlist listing utility based on https://github.com/spotify/web-api-auth-examples. It uses Spotify API to handle user login and to fetch album information.

### Usage instructions: ###
1. Clone the repo and navigate to its directory.
2. Install dependencies ('npm install').
3. Run the application ('node app.js').
4. Navigate to localhost:878 in your web browser.